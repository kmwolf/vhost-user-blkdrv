use memfd::MemfdOptions;
use memmap::MmapMut;
use std::os::unix::io::AsRawFd;
use virtio_driver::{Completion, VhostUser, VirtioBlk};

fn main() {
    println!("Hello, world!");

    const QUEUE_SIZE: u16 = 128;
    const MEM_SIZE: u64 = 0x10_0000;

    let mem_file = MemfdOptions::new().create("guest-ram").unwrap().into_file();
    mem_file.set_len(MEM_SIZE).unwrap();
    let mut mem = unsafe { MmapMut::map_mut(&mem_file) }.unwrap();

    let vhost = VhostUser::new("/tmp/vhost.sock").unwrap();
    let mut blk = VirtioBlk::new(vhost, 2, QUEUE_SIZE).unwrap();
    blk.add_mem_region(mem.as_ptr() as usize, mem.len(), mem_file.as_raw_fd(), 0)
        .unwrap();

    // Get config (block device size etc.)
    let cfg_blk = blk.get_config().unwrap();
    println!("Capacity: {}", 512 * cfg_blk.capacity.to_native());

    // Send read request, receive reply

    let buf = &mut mem.as_mut()[0..512];
    blk.read(0, 0, buf, 0).unwrap();

    // Make sure to wait for the reply to the read request below
    println!("Waiting for reply.");
    let mut in_flight = 1;
    while in_flight > 0 {
        let ret = blk.get_completion_fd().read();
        if ret.is_err() {
            continue;
        }

        let completions: Vec<Completion> = blk.completions(0).collect();
        for c in completions {
            println!("Completed req {}, ret {}", c.user_data, c.ret);

            for (i, byte) in buf.iter().enumerate() {
                if i % 16 == 0 {
                    println!();
                }
                print!("{:02x} ", byte);
            }
            println!();

            let i = c.user_data + 1;
            if i < 2 {
                blk.read(0, (i * 512) as u64, buf, i).unwrap();
            } else {
                in_flight -= 1;
            }
        }
    }

    println!("Done.");
}
