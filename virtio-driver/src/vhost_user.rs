use crate::virtqueue::{Virtqueue, VirtqueueLayout};
use crate::VirtioTransport;
use memfd::MemfdOptions;
use memmap::MmapMut;
use std::fs::File;
use std::io::{Error, ErrorKind};
use std::mem;
use std::os::unix::io::AsRawFd;
use std::os::unix::io::RawFd;
use vhost::vhost_user::message::VhostUserConfigFlags;
use vhost::vhost_user::{Master, VhostUserMaster};
use vhost::{VhostBackend, VhostUserMemoryRegionInfo, VringConfigData};
use vm_memory::ByteValued;
use vmm_sys_util::eventfd::EventFd;

#[derive(Debug)]
pub struct VhostUserBlkError(std::io::Error);

impl<E: 'static + std::error::Error + Send + Sync> From<E> for VhostUserBlkError {
    fn from(e: E) -> Self {
        VhostUserBlkError(Error::new(ErrorKind::Other, e))
    }
}

pub struct VhostUser {
    vhost: Master,
    max_queues: usize,
    mem_table: Vec<VhostUserMemoryRegionInfo>,
    mem_file: File,
    mmap: Option<MmapMut>,
    eventfd_kick: EventFd,
    eventfd_call: EventFd,
}

impl VhostUser {
    pub fn new(path: &str) -> Result<VhostUser, VhostUserBlkError> {
        let mut vhost = Master::connect(path, 0)?;

        vhost.set_owner()?;

        // FIXME Proper feature negotiation
        let _features = vhost.get_features()?;
        vhost.set_features(1 << 32)?;

        let vhost_features = vhost.get_protocol_features()?;
        vhost.set_protocol_features(vhost_features)?;

        let max_queues = vhost.get_queue_num()?.try_into()?;

        // TODO Should this be per queue?
        let eventfd_kick = EventFd::new(0).unwrap();
        let eventfd_call = EventFd::new(0).unwrap();

        let memfd = MemfdOptions::new().create("virtio-ring")?;
        let mem_file = memfd.into_file();

        let vu = VhostUser {
            vhost,
            max_queues,
            mem_table: Vec::new(),
            mem_file,
            mmap: None,
            eventfd_kick,
            eventfd_call,
        };

        Ok(vu)
    }

    fn setup_queue<R: Copy>(
        &mut self,
        i: usize,
        q: &Virtqueue<&mut [u8], R>,
    ) -> Result<(), vhost::Error> {
        let vhost = &mut self.vhost;
        vhost.set_vring_num(i, q.queue_size())?;
        vhost.set_vring_base(i, 0)?;
        vhost.set_vring_addr(
            i,
            &VringConfigData {
                queue_max_size: q.queue_size(),
                queue_size: q.queue_size(),
                flags: 0,
                desc_table_addr: q.desc_table_ptr() as u64,
                avail_ring_addr: q.avail_ring_ptr() as u64,
                used_ring_addr: q.used_ring_ptr() as u64,
                log_addr: None,
            },
        )?;

        vhost.set_vring_kick(0, &self.eventfd_kick)?;
        vhost.set_vring_call(0, &self.eventfd_call)?;
        Ok(())
    }
}

impl<'a> VirtioTransport<'a> for VhostUser {
    type Memory = &'a mut [u8];

    fn max_queues(&self) -> usize {
        self.max_queues
    }

    fn alloc_queue_mem(&mut self, layout: &VirtqueueLayout) -> Result<&mut [u8], Error> {
        self.mem_file.set_len(
            layout
                .num_queues
                .checked_mul(layout.end_offset)
                .ok_or_else(|| Error::new(ErrorKind::InvalidInput, "Queue is too large"))?
                as u64,
        )?;

        let mmap = unsafe { MmapMut::map_mut(&self.mem_file) }?;

        self.add_mem_region(
            mmap.as_ptr() as usize,
            mmap.len(),
            self.mem_file.as_raw_fd(),
            0,
        )?;

        self.mmap = Some(mmap);
        Ok(self.mmap.as_mut().unwrap().as_mut())
    }

    fn add_mem_region(
        &mut self,
        addr: usize,
        len: usize,
        fd: RawFd,
        fd_offset: i64,
    ) -> Result<(), Error> {
        let mmap_offset = u64::try_from(fd_offset)
            .map_err(|_| Error::new(ErrorKind::InvalidInput, "Invalid fd_offset"))?;

        self.mem_table.push(VhostUserMemoryRegionInfo {
            guest_phys_addr: addr as u64,
            memory_size: len as u64,
            userspace_addr: addr as u64,
            mmap_offset,
            mmap_handle: fd,
        });
        self.vhost
            .set_mem_table(&self.mem_table)
            .map_err(|e| Error::new(ErrorKind::Other, e))?;
        Ok(())
    }

    fn setup_queues<R: Copy>(&mut self, queues: &[Virtqueue<&mut [u8], R>]) -> Result<(), Error> {
        for (i, q) in queues.iter().enumerate() {
            self.setup_queue(i, q)
                .map_err(|e| Error::new(ErrorKind::Other, e))?;
        }
        Ok(())
    }

    fn get_config<C: ByteValued>(&mut self) -> Result<C, Error> {
        let cfg_size: usize = mem::size_of::<C>();
        let buf = vec![0u8; cfg_size];
        let (_cfg, cfg_payload) = self
            .vhost
            .get_config(0, cfg_size as u32, VhostUserConfigFlags::empty(), &buf)
            .map_err(|e| Error::new(ErrorKind::Other, e))?;

        Ok(*C::from_slice(&cfg_payload).unwrap())
    }

    fn kick(&self) {
        self.eventfd_kick.write(1).unwrap();
    }

    fn get_completion_fd(&self) -> &EventFd {
        &self.eventfd_call
    }
}
