use std::io::Error;
use std::ops::DerefMut;
use std::os::unix::io::RawFd;
use vm_memory::ByteValued;
use vmm_sys_util::eventfd::EventFd;

mod vhost_user;
mod virtio_blk;
mod virtqueue;

pub use vhost_user::VhostUser;
pub use virtio_blk::VirtioBlk;

use virtqueue::{Virtqueue, VirtqueueLayout};

pub struct Completion {
    pub user_data: usize,
    pub ret: i32,
}

pub trait VirtioTransport<'a> {
    type Memory: DerefMut<Target = [u8]> + 'a;

    fn max_queues(&self) -> usize;

    fn alloc_queue_mem(&'a mut self, layout: &VirtqueueLayout) -> Result<Self::Memory, Error>;

    fn add_mem_region(
        &mut self,
        addr: usize,
        len: usize,
        fd: RawFd,
        fd_offset: i64,
    ) -> Result<(), Error>;

    fn setup_queues<R: Copy>(&mut self, queues: &[Virtqueue<&mut [u8], R>]) -> Result<(), Error>;

    fn get_config<C: ByteValued>(&mut self) -> Result<C, Error>;

    fn kick(&self);
    fn get_completion_fd(&self) -> &EventFd;
}
