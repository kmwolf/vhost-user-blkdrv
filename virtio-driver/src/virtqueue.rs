use std::io::{Error, ErrorKind};
use std::mem;
use std::num::Wrapping;
use std::ops::DerefMut;
use std::sync::atomic::{AtomicU16, Ordering};
use vm_memory::{Le16, Le32, Le64};

#[repr(C, packed)]
#[allow(dead_code)]
struct VirtqueueDescriptor {
    addr: Le64,
    len: Le32,
    flags: Le16,
    next: Le16,
}

#[repr(C, packed)]
#[derive(Copy, Clone)]
struct VirtqueueUsedElem {
    idx: Le32,
    _len: Le32,
}

pub struct VirtqueueLayout {
    pub num_queues: usize,
    pub avail_offset: usize,
    pub used_offset: usize,
    pub req_offset: usize,
    pub end_offset: usize,
}

impl VirtqueueLayout {
    pub fn new<R>(num_queues: usize, queue_size: usize) -> Result<Self, Error> {
        let desc_bytes = mem::size_of::<VirtqueueDescriptor>() * queue_size;
        let avail_bytes = 8 + mem::size_of::<Le16>() * queue_size;
        let used_bytes = 8 + mem::size_of::<VirtqueueUsedElem>() * queue_size;
        let req_bytes = mem::size_of::<R>() * queue_size;

        if avail_bytes % 4 != 0 {
            return Err(Error::new(ErrorKind::InvalidInput, "Invalid queue size"));
        }

        Ok(VirtqueueLayout {
            num_queues,
            avail_offset: desc_bytes,
            used_offset: desc_bytes + avail_bytes,
            req_offset: desc_bytes + avail_bytes + used_bytes,
            end_offset: desc_bytes + avail_bytes + used_bytes + req_bytes,
        })
    }
}

struct VirtqueueRing<'a, T: Clone> {
    ptr: *const u8,
    next_idx: Wrapping<u16>,
    remote_next_idx: &'a mut AtomicU16,
    ring: &'a mut [T],
}

impl<'a, T: Clone> VirtqueueRing<'a, T> {
    fn new(mem: &'a mut [u8], queue_size: usize) -> Result<Self, Error> {
        if mem.len() < 4 + queue_size * mem::size_of::<T>() {
            return Err(Error::new(
                ErrorKind::InvalidInput,
                "mem too small for queue size",
            ));
        }

        Ok(VirtqueueRing {
            ptr: &mut mem[0] as *mut u8,
            next_idx: Wrapping(0),
            ring: unsafe {
                std::slice::from_raw_parts_mut(&mut mem[4] as *mut u8 as *mut T, queue_size)
            },
            remote_next_idx: unsafe { &mut *(&mut mem[2] as *mut u8 as *mut AtomicU16) },
        })
    }

    fn ring_idx(&self) -> usize {
        self.next_idx.0 as usize % self.ring.len()
    }

    fn push(&mut self, elem: T) {
        self.ring[self.ring_idx()] = elem;
        self.next_idx += Wrapping(1);
    }

    fn pop(&mut self) -> Option<T> {
        let remote_next_idx = self.load_next_idx();

        if remote_next_idx != self.next_idx.0 {
            let result = self.ring[self.ring_idx()].clone();
            self.next_idx += Wrapping(1);
            Some(result)
        } else {
            None
        }
    }

    fn store_next_idx(&self) {
        self.remote_next_idx
            .store(self.next_idx.0.to_le(), Ordering::Release);
    }

    fn load_next_idx(&self) -> u16 {
        u16::from_le(self.remote_next_idx.load(Ordering::Acquire))
    }

    fn num_pending(&self) -> usize {
        let remote_next_idx = self.load_next_idx() as usize;
        let ring_len = self.ring.len();
        (remote_next_idx + ring_len - self.ring_idx()) % ring_len
    }
}

pub struct Virtqueue<'a, M, R: Copy> {
    #[allow(dead_code)]
    mem: M,
    queue_size: u16,
    avail: VirtqueueRing<'a, Le16>,
    used: VirtqueueRing<'a, VirtqueueUsedElem>,
    desc: &'a mut [VirtqueueDescriptor],
    req: &'a mut [R],
    first_free_desc: u16,
}

pub struct BufferElement {
    pub addr: u64,
    pub len: u32,
    pub write: bool,
}

pub struct VirtqueueCompletion<R> {
    pub idx: u16,
    pub req: R,
}

const NO_FREE_DESC: u16 = 0xffff;

impl<'a, M, R: Copy> Virtqueue<'a, M, R>
where
    M: DerefMut<Target = [u8]> + 'a,
{
    pub fn new(mut buf: M, queue_size: u16) -> Result<Self, Error> {
        let layout = VirtqueueLayout::new::<R>(1, queue_size as usize)?;
        if buf.len() != layout.end_offset {
            return Err(Error::new(
                ErrorKind::InvalidInput,
                "Incorrectly sized queue buffer",
            ));
        }

        let mem = unsafe { std::slice::from_raw_parts_mut(&mut buf[0] as *mut u8, buf.len()) };

        let (mem, req_mem) = mem.split_at_mut(layout.req_offset);
        let (mem, used_mem) = mem.split_at_mut(layout.used_offset);
        let (desc_mem, avail_mem) = mem.split_at_mut(layout.avail_offset);

        let avail = VirtqueueRing::new(avail_mem, queue_size as usize);
        let used = VirtqueueRing::new(used_mem, queue_size as usize);

        let desc: &mut [VirtqueueDescriptor] = unsafe {
            std::slice::from_raw_parts_mut(
                desc_mem.as_ptr() as *mut VirtqueueDescriptor,
                queue_size as usize,
            )
        };
        for i in 0..queue_size - 1 {
            desc[i as usize].next = (i + 1).into();
        }
        desc[(queue_size - 1) as usize].next = NO_FREE_DESC.into();

        let req: &mut [R] = unsafe {
            std::slice::from_raw_parts_mut(req_mem.as_ptr() as *mut R, queue_size as usize)
        };

        Ok(Virtqueue {
            mem: buf,
            queue_size,
            desc,
            avail: avail?,
            used: used?,
            req,
            first_free_desc: 0,
        })
    }

    pub fn queue_size(&self) -> u16 {
        self.queue_size
    }

    pub fn desc_table_ptr(&self) -> *const u8 {
        self.desc as *const [_] as *const u8
    }

    pub fn avail_ring_ptr(&self) -> *const u8 {
        self.avail.ptr
    }

    pub fn used_ring_ptr(&self) -> *const u8 {
        self.used.ptr
    }

    fn add_avail(&mut self, desc: &[u16]) -> Result<(), Error> {
        for &d in desc {
            if d >= self.queue_size {
                return Err(Error::new(
                    ErrorKind::InvalidInput,
                    "Invalid descriptor index",
                ));
            }

            self.avail.push(d.into())
        }

        self.avail.store_next_idx();
        Ok(())
    }

    fn add_desc(&mut self, buf: &[BufferElement]) -> Result<u16, Error> {
        let first_idx = self.first_free_desc;

        for (i, b) in buf.iter().enumerate() {
            let mut flags = 0;

            if b.write {
                flags |= 0x2;
            }
            if i + 1 < buf.len() {
                flags |= 0x1;
            }

            if self.first_free_desc == NO_FREE_DESC {
                // The free descriptor chain is unchanged, so just resetting first_free_desc
                // reverts everything to the initial state
                self.first_free_desc = first_idx;
                return Err(Error::new(
                    ErrorKind::OutOfMemory,
                    "Not enough free descriptors",
                ));
            }

            let next_free_desc = self.desc[self.first_free_desc as usize].next;
            self.desc[self.first_free_desc as usize] = VirtqueueDescriptor {
                addr: b.addr.into(),
                len: b.len.into(),
                flags: flags.into(),
                next: next_free_desc,
            };
            self.first_free_desc = next_free_desc.into();
        }

        Ok(first_idx)
    }

    pub fn add_request<F>(&mut self, req: R, buffers: F) -> Result<u16, Error>
    where
        F: FnOnce(&R) -> Vec<BufferElement>,
    {
        let next_desc_idx = self.first_free_desc as usize;
        self.req[next_desc_idx] = req;

        let desc_idx = self.add_desc(&buffers(&self.req[next_desc_idx]))?;
        assert!(next_desc_idx == desc_idx as usize);

        self.add_avail(&[desc_idx])?;
        Ok(desc_idx)
    }

    fn free_desc(&mut self, first_idx: u16) {
        let mut idx = first_idx as usize;
        while self.desc[idx].flags.to_native() & 0x1 != 0 {
            idx = self.desc[idx].next.to_native().into();
        }

        self.desc[idx].next = self.first_free_desc.into();
        self.first_free_desc = first_idx;
    }

    pub fn completions(&mut self) -> VirtqueueIter<'_, 'a, M, R> {
        VirtqueueIter { virtqueue: self }
    }
}

pub struct VirtqueueIter<'a, 'queue, M, R: Copy> {
    virtqueue: &'a mut Virtqueue<'queue, M, R>,
}

impl<'a, 'queue, M, R: Copy> Iterator for VirtqueueIter<'a, 'queue, M, R>
where
    M: DerefMut<Target = [u8]> + 'queue,
{
    type Item = VirtqueueCompletion<R>;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.virtqueue.used.pop()?;
        let idx = (next.idx.to_native() % (self.virtqueue.queue_size as u32)) as u16;
        self.virtqueue.free_desc(idx);

        Some(VirtqueueCompletion {
            idx,
            req: self.virtqueue.req[idx as usize],
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = self.len();
        (len, Some(len))
    }
}

impl<'a, 'queue, M, R: Copy> ExactSizeIterator for VirtqueueIter<'a, 'queue, M, R>
where
    M: DerefMut<Target = [u8]> + 'queue,
{
    fn len(&self) -> usize {
        self.virtqueue.used.num_pending()
    }
}
