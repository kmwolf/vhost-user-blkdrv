use crate::virtqueue::{BufferElement, Virtqueue, VirtqueueLayout};
use crate::{Completion, VirtioTransport};
use libc::{c_void, iovec};
use std::collections::HashMap;
use std::io::{Error, ErrorKind};
use std::mem;
use std::os::unix::io::RawFd;
use vm_memory::{ByteValued, Le16, Le32, Le64};
use vmm_sys_util::eventfd::EventFd;

#[derive(Clone, Copy, Default)]
#[repr(packed)]
pub struct VirtioBlkConfig {
    pub capacity: Le64,
    pub size_max: Le32,
    pub seg_max: Le32,
    pub cylinders: Le16,
    pub heads: u8,
    pub sectors: u8,
    pub blk_size: Le32,
    pub physical_block_exp: u8,
    pub aligment_offset: u8,
    pub min_io_size: Le16,
    pub opt_io_size: Le32,
    pub writeback: u8,
    _unused0: [u8; 3],
    pub max_discard_sectors: Le32,
    pub max_discard_seg: Le32,
    pub discard_sector_alignment: Le32,
    pub max_write_zeroes_sectors: Le32,
    pub max_write_zeroes_seg: Le32,
    pub write_zeroes_may_unmap: u8,
    _unused1: [u8; 3],
}

unsafe impl ByteValued for VirtioBlkConfig {}

#[derive(Clone, Copy, Default)]
#[repr(packed)]
#[allow(dead_code)]
pub struct VirtioBlkReqHeader {
    req_type: Le32,
    _reserved: Le32,
    sector: Le64,
}

#[derive(Clone, Copy, PartialEq)]
#[repr(u32)]
pub enum VirtioBlkReqType {
    Read = 0,
    Write = 1,
    Flush = 4,
    Discard = 11,
    WriteZeroes = 13,
}

impl VirtioBlkReqType {
    fn is_write(&self) -> bool {
        *self != Self::Read
    }
}

impl VirtioBlkReqHeader {
    pub fn new(req_type: VirtioBlkReqType, offset: u64) -> Self {
        Self {
            req_type: (req_type as u32).into(),
            _reserved: 0.into(),
            sector: offset.into(),
        }
    }
}

unsafe impl ByteValued for VirtioBlkReqHeader {}

struct VirtioBlkRequest {
    user_data: usize,
}

#[derive(Clone, Copy)]
struct VirtioBlkReqBuf {
    header: VirtioBlkReqHeader,
    status: u8,
}

pub struct VirtioBlk<'a, T: for<'b> VirtioTransport<'b>> {
    transport: T,
    queues: Vec<Virtqueue<'a, &'a mut [u8], VirtioBlkReqBuf>>,
    requests: HashMap<u16, VirtioBlkRequest>,
}

impl<'a, T: for<'b> VirtioTransport<'b>> VirtioBlk<'a, T> {
    pub fn new(mut transport: T, num_queues: usize, queue_size: u16) -> Result<Self, Error> {
        if transport.max_queues() < num_queues {
            return Err(Error::new(
                ErrorKind::InvalidInput,
                "Too many queues requested",
            ));
        }

        let layout = VirtqueueLayout::new::<VirtioBlkReqBuf>(num_queues, queue_size as usize)?;
        let queues: Vec<_> = {
            let mut mem = transport.alloc_queue_mem(&layout)?;
            (0..num_queues)
                .map(|i| {
                    // FIXME Could safely split the slice
                    let mem_queue = unsafe {
                        std::slice::from_raw_parts_mut(
                            &mut mem[i * layout.end_offset] as *mut u8,
                            layout.end_offset,
                        )
                    };
                    Virtqueue::new(mem_queue, queue_size)
                })
                .collect::<Result<_, _>>()?
        };
        transport.setup_queues(&queues)?;

        Ok(VirtioBlk {
            transport,
            queues,
            requests: HashMap::new(),
        })
    }

    pub fn add_mem_region(
        &mut self,
        addr: usize,
        len: usize,
        fd: RawFd,
        fd_offset: i64,
    ) -> Result<(), Error> {
        self.transport.add_mem_region(addr, len, fd, fd_offset)
    }

    pub fn get_config(&mut self) -> Result<VirtioBlkConfig, Error> {
        self.transport.get_config()
    }

    fn queue_request(
        &mut self,
        q_idx: usize,
        req_type: VirtioBlkReqType,
        offset: u64,
        buf: &[iovec],
        user_data: usize,
    ) -> Result<(), Error> {
        // FIXME Need to get this from the configuration
        let block_size = 512;
        let lba = offset
            .checked_div(block_size)
            .ok_or_else(|| Error::new(ErrorKind::InvalidInput, "Unaligned request"))?;

        let req = VirtioBlkReqBuf {
            header: VirtioBlkReqHeader::new(req_type, lba),
            status: 0,
        };

        let q = &mut self.queues[q_idx];
        let desc_idx = q.add_request(req, |r| {
            let mut bufs = Vec::with_capacity(buf.len() + 2);

            bufs.push(BufferElement {
                addr: &r.header as *const VirtioBlkReqHeader as u64,
                len: mem::size_of::<VirtioBlkReqHeader>() as u32,
                write: false,
            });

            for b in buf {
                bufs.push(BufferElement {
                    addr: b.iov_base as u64,
                    len: b.iov_len as u32, // FIXME check cast
                    write: !req_type.is_write(),
                });
            }

            bufs.push(BufferElement {
                addr: &r.status as *const u8 as u64,
                len: 1,
                write: true,
            });

            bufs
        })?;

        self.requests
            .insert(desc_idx, VirtioBlkRequest { user_data });

        Ok(())
    }

    pub fn read(
        &mut self,
        q_idx: usize,
        offset: u64,
        buf: &mut [u8],
        user_data: usize,
    ) -> Result<(), Error> {
        let iov = iovec {
            iov_base: buf.as_mut_ptr() as *mut c_void,
            iov_len: buf.len(),
        };

        self.queue_request(q_idx, VirtioBlkReqType::Read, offset, &[iov], user_data)?;
        self.transport.kick();

        Ok(())
    }

    pub fn write(
        &mut self,
        q_idx: usize,
        offset: u64,
        buf: &mut [u8],
        user_data: usize,
    ) -> Result<(), Error> {
        let iov = iovec {
            iov_base: buf.as_mut_ptr() as *mut c_void,
            iov_len: buf.len(),
        };

        self.queue_request(q_idx, VirtioBlkReqType::Write, offset, &[iov], user_data)?;
        self.transport.kick();

        Ok(())
    }

    pub fn get_completion_fd(&self) -> &EventFd {
        self.transport.get_completion_fd()
    }

    pub fn completions(&mut self, q_idx: usize) -> Box<dyn Iterator<Item = Completion> + '_> {
        let q = &mut self.queues[q_idx];
        Box::new(q.completions().filter_map(|completion| {
            if let Some(req) = self.requests.remove(&completion.idx) {
                Some(Completion {
                    user_data: req.user_data,
                    // FIXME Map to errno
                    ret: completion.req.status as i32,
                })
            } else {
                println!(
                    "Backend sent completion for unknown request {}",
                    completion.idx
                );
                None
            }
        }))
    }
}
